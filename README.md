Role Name
=========

Install MS teams

https://www.microsoft.com/de-at/microsoft-teams/group-chat-software

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: desktop
      roles:
         - dietrichliko.teams

License
-------

MIT

Author Information
------------------

Dietrich.Liko@oeaw.ac.at
